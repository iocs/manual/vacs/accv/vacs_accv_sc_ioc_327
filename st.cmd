epicsEnvSet(ECMC_VER, "6.3.3")

#
# Require module: essioc
#
require essioc

#
# Require module: ecmccfg
# Require module: ecmc
#
require ecmccfg $(ECMC_VER)
require ecmc $(ECMC_VER)


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")


#
# IOC: VacS-ACCV:Ctrl-IOC-327
# Load iocsh file
#
iocshLoad("$(E3_CMD_TOP)/iocsh/ecat_setup.iocsh")
