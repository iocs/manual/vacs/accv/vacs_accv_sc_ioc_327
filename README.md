# IOC for Fast Data Acquisition of vacuum gauges

## Used modules

*   [ecmccfg](https://gitlab.esss.lu.se/e3/wrappers/ecat/e3-ecmccfg)
*   [ecmc](https://gitlab.esss.lu.se/e3/wrappers/ecat/e3-ecmc)


## Controlled devices

*   LEBT-010:Vac-ECATIO-10001
*   RFQ-010:Vac-ECATIO-10002
*   MEBT-010:Vac-ECATIO-10003
*   DTL-010:Vac-ECATIO-10004
